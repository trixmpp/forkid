#include <TinyGPS++.h>
#include <Process.h>
#include <AltSoftSerial.h>

#include <SoftwareSerial.h>

SoftwareSerial sLCD =  SoftwareSerial(3, 6); /* Serial LCD is connected on pin 14 (Analog input 0) */
#define COMMAND 0xFE
#define CLEAR   0x01
#define LINE0   0x80
#define LINE1   0xC0

//#define INPUT_CAPTURE_PIN = 8;

static const int LEDPIN = 8;
// Settings for Maplin GPS sheild
//static const int RXPin = 2, TXPin = 3, RXJumperPin = 8;
//static const uint32_t GPSBaud = 9600;

// Settings for sparkfun canbus shield
static const int RXPin = 4, TXPin = 5, RXJumperPin = 8;
static const uint32_t GPSBaud = 4800;

// The TinyGPS++ object
TinyGPSPlus gps;

// The serial connection to the GPS device
AltSoftSerial ss;

String inputString = "";         // a string to hold incoming data
boolean stringComplete = false;  // whether the string is complete
Process p;

void setup() {
  pinMode(RXPin, INPUT); // Because we are jumping from RXpin to RXJumper pin. Must be input so we don't short the jumper out.
  pinMode(LEDPIN, OUTPUT);
     sLCD.begin(9600);
   clearLCD();
   
  ss.begin(GPSBaud);
  inputString.reserve(200);
  
  Bridge.begin();
  //Serial.begin(9600);
          sLCD.write(byte(COMMAND));                   /* Move LCD cursor to line 0 */
        sLCD.write(byte(LINE0));
        sLCD.write("Works if see me!");
}


// Main loop. 
void loop() {

  // Delay whilst receiving data from the software Serial. Approx 5 second between updates with curl delay.
  smartDelay(1000);
  
  // Send the most recent data to firebase.
  //Serial.print("~"); Serial.flush(); delay(50);
  if (gps.location.isValid() && gps.location.age() < 1500) {
    sendGPSDataToFirebase("testForkID1", gpsTimeString(), String(gps.location.lat(),6), String(gps.location.lng(),6), String(gps.satellites.value()));
    digitalWrite(LEDPIN, !digitalRead(LEDPIN)); // Blink light to ensure operation
    clearLCD();
    writeLCD("Lat:" + String(gps.location.lat(),6), LINE0);
    writeLCD("Long:" + String(gps.location.lng(),6), LINE1);
  sLCD.write(byte(LINE1));
  sLCD.print(String(gps.location.lat(),6));
  } else {
    digitalWrite(LEDPIN, !digitalRead(LEDPIN));
    delay(10);
    digitalWrite(LEDPIN, !digitalRead(LEDPIN));
        clearLCD();
    writeLCD("NO GPS", LINE0);
    writeLCD("NO GPS", LINE1);
  }
}

void writeLCD(String str, byte line) {
        sLCD.write(byte(COMMAND));                   /* Move LCD cursor to line 0 */
        sLCD.write(line);
        sLCD.print(str);
        /*
        unsigned int len = str.length();
        char buf[len];
        str.toCharArray(buf,len);
        sLCD.write(buf);
        */
}

void sendGPSDataToFirebase(String deviceID, String timestamp, String lat, String lon, String nSat) {
  sendStringToFirebase( "\"id\": \"" + deviceID + "\", \"timestamp\": \"" + timestamp + "\", \"lat\": \"" + lat + "\", \"long\": \"" + lon + "\", \"nSat\": \"" + nSat + "\"" );
}

void sendStringToFirebase(String str) {
  //strToSend = "curl --connect-timeout 1 --max-time 1 -k -X POST https://forkiddemo.firebaseio.com/.json -d '{" + str + "}'";
  //String strToSend = "ping -c 1 74.125.230.159";
  //Serial.println(str);
  //Serial.println(strToSend);
  //Serial.print("*");  Serial.flush(); delay(50);
  if( p.running() ) {
    p.close(); // Kill old process if it is still running.
    p.flush();
    //Serial.println("killed process");
  }
  //Serial.print("A"); Serial.flush(); delay(50);
  p.runShellCommand("curl --connect-timeout 5 --max-time 5 -k -X POST https://forkiddemo.firebaseio.com/.json -d '{" + str + "}'");
  //p.flush();
  //Serial.print("B"); Serial.flush(); delay(50);
  int i = 0;
  while( p.running() && i < 10 ) {
    delay(100);
    //Serial.print("."); Serial.flush();
  }
  //delay(50);
  //Serial.print("-"); Serial.flush();
  //delay(50);
  //p.available(); // I think it locks up here.
  //delay(50);
  //Serial.print("+"); Serial.flush();
  //delay(50);
 // while(p.available()) {
  //  Serial.print(",");
 //   Serial.print((char)p.read());
 // }
 // Serial.print("|");
 // int exitValue = p.exitValue();
 // Serial.print(", Exit: " + String(exitValue));
 // Serial.println(",");
}

String gpsTimeString() {
  char ts[64];
  sprintf(ts, "%02d/%02d/%02dT%02d:%02d:%02d", gps.date.day(), gps.date.month(), gps.date.year(), gps.time.hour(), gps.time.minute(), gps.time.second());
  return String(ts);
}

// This custom version of delay() ensures that the gps object
// is being "fed".
static void smartDelay(unsigned long ms)
{
  unsigned long start = millis();
 // Serial.print(" d");
  do
  {
    Serial.print("_");
    while (ss.available()) {
      
      char r = ss.read();
      gps.encode(r);
    }
    delay(100);
  } while (millis() - start < ms);
}

void clearLCD(){
   sLCD.write(COMMAND);   //command flag
   sLCD.write(0x01);   //clear command.
   delay(10);
}
